/*---------------------------------------------------------------------
 *
 * my_fortune.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Fortune Telling Homework 3
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>

/* User */
#include "log_mgr.h"
#include "my_fortune.h"

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: 
 * Outputs:
 * Note: 
 * 
----------------------------------------------------------------------*/

// TODO: Change these to direct calls inline and print log messages.
void
set_fl(int fd, int flags) /* flags are file status flags to turn on */
{
        int             val;

        if ( (val = fcntl(fd, F_GETFL, 0)) < 0)
                printf("F_GETFL error.\n");
//                err_sys("fcntl F_GETFL error");

        val |= flags;           /* turn on flags */

        if (fcntl(fd, F_SETFL, val) < 0)
                printf("F_SETFL error.\n");
//                err_sys("fcntl F_SETFL error");
}

void
clr_fl(int fd, int flags)
                                /* flags are the file status flags to turn off */
{
        int             val;

        if ( (val = fcntl(fd, F_GETFL, 0)) < 0)
                printf("F_GETFL error.\n");
//                err_sys("fcntl F_GETFL error");

        val &= ~flags;          /* turn flags off */

        if (fcntl(fd, F_SETFL, val) < 0)
                printf("F_SETFL error.\n");
//                err_sys("fcntl F_SETFL error");
}

void catchCtrlC(int placeHolder)
{
    printf("Caught c. Restoring file I/O.\n");
    clr_fl( STDOUT_FILENO, O_NONBLOCK );

    // TODO: Print an error log message here.
    log_event (FATAL, "Fortune program exited via Sig Int.");
    exit(ERROR);
}

int main ( int argc, char* argv[] )
{
    char buf[50000];

    /* Register sig int handler. */
    signal(SIGINT, catchCtrlC);

    // Set non-blocking I/O.
    set_fl( STDOUT_FILENO, O_NONBLOCK );

    // Seed the random number generator and get a number between 1-15.
    srand(time(NULL));
    uint8_t sleepTime = rand() % 15;

    if (0 == sleepTime)
    {
        sleepTime += 1;
    }

    // Begin loop and sleep to wait.
    while (TRUE)
    {
        FILE* fortune = NULL;
        char line[256];
        int nReadIn = 0;

        printf( "Sleeping for %d seconds.\n", sleepTime );

        // Read input in non-blocking fashion before sleeping.
        nReadIn = read( STDIN_FILENO, buf, sizeof(buf) );
        if ( nReadIn > 0 )
        {
            if ( buf[0] == 'q' )
            {
                break;
            }
            printf("Input: %s.\n", buf);
        }
        sleep( sleepTime );

        // Call to popen here. How do we handle the changing strings?
        // FILE* popen( const char* cmdstring, const char* type);
        fortune = popen( "/home/unix_dev/NOTES/fortune_absaroka/fortune", "r");
        while ( fgets (line, 256, fortune) != NULL )
        {
            uint16_t i = 0;
            for( i = 0; i < strlen(line); i += 1 )
            {
                if ( islower( line[i] ) )
                {
                    line[i] = toupper(line[i]);
                }
            }
            printf("%s", line);
        }

        // Generate a new random number for sleeping.
        sleepTime = rand() % 15;
    }

    // Clear non-blocking I/O.
    clr_fl( STDOUT_FILENO, O_NONBLOCK );

}
