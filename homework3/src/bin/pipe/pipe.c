/*---------------------------------------------------------------------
 *
 * pipe.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Fortune Telling Homework 3
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

/* User */
#include "log_mgr.h"
#include "pipe.h"

/*----------------------------- Globals ------------------------------*/

extern char** environ;

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: Two arguments encased in quotes.
 * Outputs: The output from first argument piped to second.
 * Note: 
 * 
----------------------------------------------------------------------*/

int main ( int argc, char* argv[] )
{
    int i = 0;
    int j = 0;
    int pfd[2];
    char* token_arg1[10];
    char* token_arg2[10];
    int num_tokens_in_string;

    /* Copy argv into a modifiable array. */
    char* argv1_array = NULL;
    char* argv2_array = NULL;

    pid_t pid;
    int execve_return = 0;

    if (argc != 3)
    {
        // TODO: Handle this error in prints.
        return ERROR;
    }

    /* Parse first argument and flags. */
    argv1_array = (char*) malloc(strlen(argv[1]));
    strcpy(argv1_array, argv[1]);

    token_arg1[0] = strtok (argv1_array, " ");

    for (num_tokens_in_string = 1;
                    num_tokens_in_string < NUM_TOKENS;
                    num_tokens_in_string++)
    {
            token_arg1[num_tokens_in_string] = strtok (NULL, " \011");
            if (token_arg1[num_tokens_in_string] == NULL)
                    break;
    }

    /* Parse second argument and flags. */
    argv2_array = (char*) malloc(strlen(argv[2]));
    strcpy(argv2_array, argv[2]);

    token_arg2[0] = strtok (argv2_array, " ");

    for (num_tokens_in_string = 1;
                    num_tokens_in_string < NUM_TOKENS;
                    num_tokens_in_string++)
    {
            token_arg2[num_tokens_in_string] = strtok (NULL, " \011");
            if (token_arg2[num_tokens_in_string] == NULL)
                    break;
    }

    /* Create pipe for manipulating stdin and stdout. */
    pipe(pfd);

    /* Create first child process and fix pipe. */
    if ( (pid = fork()) < 0 )
    {
        printf("fork error\n");
    }

    else if (pid == 0)
    {
        /* Closing pipe and reassigning after exec. */
        close(pfd[0]);
        dup2(pfd[1], STDOUT_FILENO);
        close(pfd[1]);
        execve_return = execvp(token_arg1[0], &token_arg1[0]);
        // TODO: Handle an error from execvp.
    }

    /* Have the parent spin off another child. */
    else if (pid > 0)
    {
        /* Create second child process and fix pipe. */
        if ( (pid = fork()) < 0 )
        {
            printf("fork error\n");
        }
    
        else if (pid == 0)
        {
            close(pfd[1]);
            dup2(pfd[0], STDIN_FILENO);
            close(pfd[0]);
            execve_return = execvp(token_arg2[0], &token_arg2[0]);
            // TODO: Print error here.
        }
    }

    // Both children running now. Call wait.
    wait();

    return SUCCESS;
}
