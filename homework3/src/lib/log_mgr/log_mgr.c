
/*---------------------------------------------------------------------
 *
 * log_mgr.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Logging Homework 2
 * Description: Library helper to create log files. 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
#include <time.h>

/* User */
#include "log_mgr.h"

/*----------------------------- Globals ------------------------------*/

// TODO-: Change these to meet global name standard.
static char* logfile = NULL;
static int logfile_descriptor = -1;

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs:
 * Outputs:
 * Note:
 * 
----------------------------------------------------------------------*/

int log_event (Levels l, const char *fmt, ...)
{
    char buffer[256];
    char dateBuffer[64];
    va_list args;
    int date_length = 0;
    int buffer_length = 0;
    int write_length = 0;
    time_t t = 0;
    struct tm tm;

    // This should probably just be a flag to signify one has been
    // opened.
    if (-1 == logfile_descriptor)
    {
        // Call set_logfile here to get things started.
        if ( set_logfile(DEFAULT_LOGFILE) )
        {
            return ERROR;
        }
    }

    t = time(NULL);
    tm = *localtime(&t);


    va_start (args, fmt);
    buffer_length = vsprintf (buffer, fmt, args);

   /* Prepare the level. */
   if (0 == l)
   {
    date_length = sprintf (dateBuffer, "%d-%d-%d %d:%d:%d:INFO:", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
   }
   else if (1 == l)
   {
    date_length = sprintf (dateBuffer, "%d-%d-%d %d:%d:%d:WARNING:", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
   }
   else if (2 == l)
   {
    date_length = sprintf (dateBuffer, "%d-%d-%d %d:%d:%d:FATAL:", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
   }
   else
   {
       printf("Error while parsing level.\n");
       return ERROR;
   }

   // Determine new size
   int newSize = date_length + buffer_length + 1; 

   // Allocate new buffer
   char* newBuffer = (char *)malloc(newSize);

   // do the copy and concat
   strcpy(newBuffer, dateBuffer);
   strcat(newBuffer, buffer);
   strcat(newBuffer, "\n");

    // Write the formatted data to logfile.
    write_length = write (logfile_descriptor, newBuffer, newSize);
    va_end (args);

   // release old buffer
   free(newBuffer);

    // Check if there was an error while writing.
    if (write_length != newSize)
    {
        // TODO: Change this return to a definition.
        // The number of written bytes does not match the desired.
        return ERROR;
    }

    // TODO: Change this return to a definition.
    return SUCCESS;

}

int set_logfile (const char *logfile_name)
{
    int new_logfile_descriptor = 0;
    // TODO: Change this return to a definition.
    int ret = SUCCESS;

    // TODO: Replace this call with open and append now.
    // TODO: Use perror/errno to check this system calls return.
    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
//    new_logfile_descriptor = creat(logfile_name, mode);
    new_logfile_descriptor = open(logfile_name, 'a', mode);

    // Open new logfile and check if successful.
    // If successful close previous logfile.
    if (new_logfile_descriptor)
    {
        if (logfile_descriptor > 0)
        {
            close_logfile();
        }
        logfile_descriptor = new_logfile_descriptor;

        // TODO: Change this return to a definition.
        ret = SUCCESS;
    }

    // Something went wrong opening new file.
    else
    {
         // TODO: Change this return to a definition.
         ret = ERROR;
    }

    // Return -1 on error.
    return ret;
}

// Close specified logfile if necessary.
void close_logfile (void)
{
    // TODO: You need some additional logic in close_logfile.
    // TODO: What if it were called when there was no logfile open?
    // Use perror and error here.
    close(logfile_descriptor);
    logfile_descriptor = -1;
}
