
/*---------------------------------------------------------------------
 *
 * log_mgr.h
 *
 *
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Logging Homework 2
 * Description: Library helper to create log files. 
 *
 *
 ---------------------------------------------------------------------*/

#ifndef MY_LOG_MGR_H
#define MY_LOG_MGR_H

/*---------------------------- Includes ------------------------------*/

/* Standard */

/* User */

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

typedef enum
{
    INFO, 
    WARNING,
    FATAL
} Levels;

#define DEFAULT_LOGFILE "logfile"
#define ERROR -1
#define SUCCESS 0

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

int log_event (Levels l, const char *fmt, ...);
int set_logfile (const char *logfile_name);
void close_logfile (void);

#endif
